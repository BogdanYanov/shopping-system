# Shopping System

#### Installation

YOU NEED DOCKER & DOCKER-COMPOSE ON YOUR COMPUTER!


* Clone this project to your computer
* Create .env file in root project directory by **.env.example**
* Go to src folder: `cd src`
* Create **products.json** by next example:
```
[
  {
    "model": "products",
    "fields":{
      "name": "Example Product 1",
      "price_cents": 1220000
    }
  },
  {
    "model": "products",
    "fields":{
      "name": "Example Product 2",
      "price_cents": 1210000,
      "created_at": "2020-08-16"
    }
  },
  {
    "model": "products",
    "fields":{
      "name": "Example Product 3",
      "price_cents": 1610000
    }
  }
]
```
* Back to root directory: `cd ..`
* Set `MYSQL_HOST` to `mysql` in **.env** for connect from python container to mysql container
* Start docker-containers:```docker-compose up -d```

It will create two containers: **mysql** and **python**. After few minutes you can check api docs in your browser. 
```
http://127.0.0.1:{FASTAPI_PORT in .env}/docs
```

#### Usage

First of all you need to authorize as admin. If you use browser docs of api, you can simply click on **Authorize** at 
the top of page, and enter `username` and `password` with values you set in **.env** (`ADMIN_USERNAME`, `ADMIN_PASSWORD`).

Then you can create accounts with different types and permissions by post method `/staff`.

If accounts already created, you can just login with values you enter, when creates account by post method `/login`.

##### API Endpoints

###### Cashier methods

##### `POST /order?product=Product`

This method create order with product name, you use in query parameter 
`product`. If `product` doesn't exist, you get 404 HTTP error.

**Request example:**
```
curl -X POST "http://localhost:7020/order?product=A" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw" -d ""
```

**Failed response example:**
```
{
  "detail": "Product doesn`t exist"
}
```

**Success response example:**
```
{
  "order_id": 1
}
```

##### `GET /bill/{order_id}`

This method generate bill for order by order_id, you use in query parameter 
`order_id`. If `order` doesn't exist, you get 404 HTTP error.

**Request example:**
```
curl -X GET "http://localhost:7020/bill/4" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw"
```

**Failed response example:**
```
{
  "detail": "Order doesn`t processed or doesn`t exist"
}
```

**Success response example:**
```
{
  "name": "IPHONE XR",
  "price": "$ 12345.00",
  "order_created_at": "2020-10-26T14:58:59",
  "bill_created_at": "2020-10-27T13:58:47.845899"
}
```

##### `PATCH /bill/{order_id}`

This method set status **PAYED** for order by order_id, you use in query parameter 
`order_id`. If `order` doesn't exist, you get 404 HTTP error.

**Request example:**
```
curl -X PATCH "http://localhost:7020/bill/4" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw"
```

**Failed response example:**
```
{
  "detail": "Order doesn`t processed or doesn`t exist"
}
```

**Success response example:**
```
{
  "message": "order was payed."
}
```

###### Seller methods

##### `GET /orders-np`

This method return orders that aren't processed yet (status **NEW**).

**Request example:**
```
curl -X GET "http://localhost:7020/orders-np" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw"
```

**Response example:**
```
{
  "orders": [
    {
      "order_id": 2,
      "created_at": "2020-10-28T15:52:42"
    }
  ]
}
```

##### `PATCH /order/{order_id}`

This method set status **PROCESSED** to order by `order_id` parameter. If `order` doesn't exist or already processed, 
you get 404 HTTP error.

**Request example:**
```
curl -X PATCH "http://localhost:7020/order/1" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw"
```

**Failed response example:**
```
{
  "detail": "Order not found, or already has been processed"
}
```

**Success response example:**
```
{
  "message": "order has been processed"
}
```


###### Accountant methods

##### `GET /all-orders?date_from={date_from}&date_to={date_to}`

This method return all orders in database for date range from `{date_from}` to `{date_to}`.
You need to enter date in format `%Y-%m-%d (Year-month-day)`.

**Request example:**
```
curl -X GET "http://localhost:7020/all-orders?date_from=2020-10-24&date_to=2020-10-28" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNDczOH0.rOUmT1Yn2nQrh6gIxWjX_AYNrSGvOCvLr7sd4_uyDQw"
```

**Response example:**
```
{
  "orders": [
    {
      "name": "IPHONE XR",
      "price": "$ 12345.00",
      "status": "new",
      "order_created_at": "2020-10-26T14:58:58"
    },
    {
      "name": "IPHONE XR",
      "price": "$ 12345.00",
      "status": "paid",
      "order_created_at": "2020-10-26T14:58:59"
    },
    {
      "name": "IPHONE XR",
      "price": "$ 12345.00",
      "status": "processed",
      "order_created_at": "2020-10-26T14:59:00"
    }
  ]
}
```

###### Overall methods

##### `POST /login`

This method authorize you into a system. You need to set `username` and `password` that you
enter when sign up. In response you get access token to set it into auth header.

**Request example:**
```
curl -X POST "http://localhost:7020/login" -H  "accept: application/json" -H  "Content-Type: application/x-www-form-urlencoded" -d "username=cashier&password=cashier"
```

**Response example:**
```
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjYXNoaWVyIiwic2NvcGVzIjpbImNhc2hpZXIiXSwiZXhwIjoxNjAzOTA2NjU0fQ.Z6VX4LSC_OCRpsepk-6VwYd9R62b92nPma0kZmLMfgk",
  "token_type": "bearer"
}
```

###### Only admin methods

##### `POST /staff?username={username}&password={password}&position={position}`

This method register staff members accounts by `username`, `password` and `position` parameters. `position` affect to permissions and can be
next: `cashier`, `seller`, `accountant`. 

**Request example:**
```
curl -X POST "http://localhost:7020/staff?username=cashier&password=cashier&position=cashier" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInNjb3BlcyI6W10sImV4cCI6MTYwMzkwNzEyMH0.pmhpUMC17vnn3pLm7QHTyjZ3nzZhJrG9ZWJtURKvIdM" -d ""
```

**Failed response example:**
```
{
  "message": "staff account already exist"
}
```

**Success response example:**
```
{
  "message": "staff account was created"
}
```