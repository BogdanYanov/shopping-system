from tools.get_price_with_sale import get_price_with_sale
from tools.order_status_map import get_order_status_map
from api.schemas import OrderExtSchema


def convert_row_to_order_schema(row) -> OrderExtSchema:
    price_str = get_price_with_sale(row.price_cents, row.product_created_at)
    status = get_order_status_map().get(row.status)
    return OrderExtSchema(name=row.name, price=price_str, status=status, order_created_at=row.order_created_at)

