from database.models.mixins import MysqlStaffPositions


def get_position_ids_map():
    return {
        'cashier': MysqlStaffPositions.POSITION_CASHIER,
        'accountant': MysqlStaffPositions.POSITION_ACCOUNTANT,
        'seller': MysqlStaffPositions.POSITION_SHOP_ASSISTANT
    }