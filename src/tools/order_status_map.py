from database.models.mixins import MysqlOrderStatus


def get_order_status_map():
    return {
        MysqlOrderStatus.STATUS_NEW: 'new',
        MysqlOrderStatus.STATUS_PROCESSED: 'processed',
        MysqlOrderStatus.STATUS_PAID: 'paid'
    }
