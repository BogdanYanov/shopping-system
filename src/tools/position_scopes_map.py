from database.models.mixins import MysqlStaffPositions


def get_position_scopes_map():
    return {
        MysqlStaffPositions.POSITION_CASHIER: ['cashier'],
        MysqlStaffPositions.POSITION_ACCOUNTANT: ['accountant'],
        MysqlStaffPositions.POSITION_SHOP_ASSISTANT: ['shop_assistant']
    }