from datetime import datetime


def get_price_with_sale(product_price: int, product_created_at: datetime) -> str:
    today = datetime.now()
    delta = today - product_created_at
    price = product_price
    if delta.days >= 30:
        price = price - (price / 100 * 20)
    return "$ {:.2f}".format(price / 100)
