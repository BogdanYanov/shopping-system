import argparse
from errors import NotJSONFileError
from pathlib import Path
import json
from api.repository import repository


class LoadProductFixtures:
    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Load fixtures for database")
        self.parser.add_argument("file", type=str, help="filepath with fixture")

    def handle(self):
        args = self.parser.parse_args()

        if not str(args.file).endswith('.json'):
            raise NotJSONFileError

        if not Path(args.file).is_file():
            raise FileNotFoundError

        with open(args.file) as fixture_file:
            data = json.load(fixture_file)
            for entry in data:
                if entry.get('model', None) == 'products':
                    repository.add_product(entry['fields'])
