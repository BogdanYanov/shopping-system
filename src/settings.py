from dotenv import load_dotenv
import os

load_dotenv()


MYSQL_DATABASE = os.getenv("MYSQL_DATABASE", "shopping-system")
MYSQL_USER = os.getenv("MYSQL_USER", "shopping-system")
MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD", "shopping-system")
MYSQL_PORT = os.getenv("MYSQL_PORT", "3306")
MYSQL_HOST = os.getenv("MYSQL_HOST", "shopping-system-mysql")

SECRET_KEY = os.getenv("SECRET_KEY", "dac6a7ee2dd1058a349d701ea39333fc07054ecf10b9fb0f6a06f092fda03d4d")
TOKEN_EXPIRE_MINUTES = int(os.getenv("TOKEN_EXPIRE_MINUTES", 30))

ADMIN_USERNAME = os.getenv("ADMIN_USERNAME", "admin")
ADMIN_PASSWORD = os.getenv("ADMIN_PASSWORD", "admin")