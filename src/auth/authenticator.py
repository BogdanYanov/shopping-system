import settings
from passlib.context import CryptContext
from auth.models import StaffInDb, TokenData
from typing import Optional, Any
from database.repository.base import BaseRepository
from datetime import timedelta, datetime
import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import SecurityScopes, OAuth2PasswordBearer
from pydantic import ValidationError


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


class Authenticator:
    ADMIN_POSITION = -1
    pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

    def __init__(self, repository):
        self._settings = settings
        self._admin = {
            'username': self._settings.ADMIN_USERNAME,
            'hashed_password': self.get_password_hash(self._settings.ADMIN_PASSWORD),
            'disable': False
        }
        self._token_expire = self._settings.TOKEN_EXPIRE_MINUTES
        self._secret_key = self._settings.SECRET_KEY
        self._algorithm = 'HS256'
        self._repository: BaseRepository = repository

    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        return self.pwd_context.verify(plain_password, hashed_password)

    def get_password_hash(self, password: str) -> str:
        return self.pwd_context.hash(password)

    def get_user(self, username: str) -> Optional[StaffInDb]:
        if username == self._admin['username']:
            return StaffInDb(**self._admin, position=self.ADMIN_POSITION)
        return self._repository.get_staff(username)

    def create_access_token(self, data: dict):
        to_encode = data.copy()
        expire = datetime.utcnow() + timedelta(minutes=self._token_expire)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, self._secret_key, algorithm=self._algorithm)
        return encoded_jwt

    def authenticate_user(self, username: str, password: str) -> Any:
        user = self.get_user(username)
        if not user:
            return False
        if not self.verify_password(password, user.hashed_password):
            return False
        return user

    async def get_current_user(self, security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
        if security_scopes.scopes:
            authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
        else:
            authenticate_value = f"Bearer"
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            payload = jwt.decode(token, self._secret_key, algorithms=[self._algorithm])
            username: str = payload.get("sub")
            if username is None:
                raise credentials_exception
            token_scopes = payload.get("scopes", [])
            token_data = TokenData(scopes=token_scopes, username=username)
        except (jwt.PyJWTError, ValidationError):
            raise credentials_exception
        user = self.get_user(username=token_data.username)
        if user is None:
            raise credentials_exception
        if user.position != self.ADMIN_POSITION:
            for scope in security_scopes.scopes:
                if scope not in token_data.scopes:
                    raise HTTPException(
                        status_code=status.HTTP_401_UNAUTHORIZED,
                        detail="Not enough permissions",
                        headers={"WWW-Authenticate": authenticate_value},
                    )
        return user

