from pydantic import BaseModel


class StaffInDb(BaseModel):
    username: str
    hashed_password: str
    position: int
