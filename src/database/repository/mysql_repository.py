from datetime import datetime
from enum import Enum
from typing import Optional, List
from errors import NoProducts
from api.schemas import BillSchema, OrdersListSchema, OrderSchema, OrderListExtSchema
from .base import BaseRepository
from sqlalchemy import select, insert, join, and_, update, delete
from database.models import Product, Order, Staff
from database.models.mixins import MysqlOrderStatus
from tools import convert_mysql_to_bill, convert_row_to_order_schema
from auth.models import StaffInDb


class MySQLRepository(BaseRepository):
    def __init__(self, engine):
        self.engine = engine

    def get_products(self) -> Enum:
        select_stmt = select([Product.name])

        res = self.engine.connect().execute(select_stmt).fetchall()

        if res:
            return Enum('Products', [(r[0], r[0]) for r in res])
        raise NoProducts

    def find_product_by_name(self, product_name: str) -> Optional[int]:
        select_stmt = select([Product.id]).where(Product.name == product_name)
        res = self.engine.connect().execute(select_stmt).fetchone()
        if res:
            return res[0]
        return None

    def add_order(self, product_id: int) -> int:
        insert_sql = insert(Order).values(
            product_id=product_id
        )

        res = self.engine.connect().execute(insert_sql)
        return res.lastrowid

    def get_bill(self, order_id: int) -> Optional[BillSchema]:
        _join = join(Product, Order, Product.id == Order.product_id)

        select_sql = select(
            [
                Product.name,
                Product.price_cents,
                Product.created_at.label("product_created_at"),
                Order.created_at.label("order_created_at"),
                Order.bill_created_at.label("bill_created_at")
            ]
        ) \
            .select_from(_join) \
            .where(and_(Order.status == MysqlOrderStatus.STATUS_PROCESSED,
                        Order.id == order_id))

        res = self.engine.connect().execute(select_sql).fetchone()
        if res:
            if res.bill_created_at is None:
                bill_created_at = datetime.now()
                self.bill_create(order_id, bill_created_at)
            else:
                bill_created_at = res.bill_created_at
            return convert_mysql_to_bill(res, bill_created_at)
        return None

    def bill_create(self, order_id: int, bill_created_at: datetime):
        update_stmt = update(Order).values(
            bill_created_at=bill_created_at
        ).where(Order.id == order_id)

        self.engine.connect().execute(update_stmt)

    def order_payed(self, order_id: int) -> bool:
        if self.check_order_exist(order_id, MysqlOrderStatus.STATUS_PROCESSED):
            update_stmt = update(Order).values(
                status=MysqlOrderStatus.STATUS_PAID
            ).where(Order.id == order_id)

            self.engine.connect().execute(update_stmt)

            return True
        return False

    def check_order_exist(self, order_id: int, status: int) -> bool:
        select_stmt = select([Order.id]).where(and_(Order.id == order_id,
                                                    Order.status == status))

        res = self.engine.connect().execute(select_stmt).fetchone()
        if res:
            return True
        return False

    def get_not_processed_orders(self) -> OrdersListSchema:
        select_stmt = select([Order.id, Order.created_at]).where(Order.status == MysqlOrderStatus.STATUS_NEW)
        res = self.engine.connect().execute(select_stmt).fetchall()
        if res:
            return OrdersListSchema(orders=[OrderSchema(order_id=r[0], created_at=r[1]) for r in res])
        return OrdersListSchema(orders=[])

    def processed_order(self, order_id: int) -> bool:
        if self.check_order_exist(order_id, MysqlOrderStatus.STATUS_NEW):
            update_stmt = update(Order).values(
                status=MysqlOrderStatus.STATUS_PROCESSED
            ).where(Order.id == order_id)

            self.engine.connect().execute(update_stmt)

            return True
        return False

    def get_orders(self, date_from: datetime, date_to: datetime) -> OrderListExtSchema:
        _join = join(Order, Product, Order.product_id == Product.id)

        select_stmt = select(
            [
                Product.name,
                Product.price_cents,
                Product.created_at.label('product_created_at'),
                Order.created_at.label('order_created_at'),
                Order.status
            ]).select_from(_join).where(and_(Order.created_at >= date_from, Order.created_at <= date_to))

        res = self.engine.connect().execute(select_stmt).fetchall()

        if res:
            return OrderListExtSchema(orders=[convert_row_to_order_schema(r) for r in res])
        return OrderListExtSchema(orders=[])

    def add_product(self, product):
        insert_stmt = insert(Product).values(**product).prefix_with("IGNORE")

        self.engine.connect().execute(insert_stmt)

    def get_staff(self, username: str) -> Optional[StaffInDb]:
        select_stmt = select([Staff.username,
                              Staff.password.label('hashed_password'),
                              Staff.position]).where(Staff.username == username)

        res = self.engine.connect().execute(select_stmt).fetchone()
        if res:
            return StaffInDb(**res)
        return None

    def create_new_staff(self, username: str, password: str, position: int) -> bool:
        if not self.get_staff(username):
            insert_stmt = insert(Staff).values(
                username=username,
                password=password,
                position=position
            )

            self.engine.connect().execute(insert_stmt)

            return True
        return False

    def delete_staff(self, username: str):
        delete_stmt = delete(Staff).where(Staff.username == username)

        self.engine.connect().execute(delete_stmt)

    def delete_product(self, product_name: str):
        delete_stmt = delete(Product).where(Product.name == product_name)

        self.engine.connect().execute(delete_stmt)

    def delete_order(self, order_id: int):
        delete_stmt = delete(Order).where(Order.id == order_id)

        self.engine.connect().execute(delete_stmt)