import abc
from typing import Optional, List
from enum import Enum
from api.schemas import BillSchema, OrdersListSchema
from datetime import datetime
from auth.models import StaffInDb


class BaseRepository(abc.ABC):
    @abc.abstractmethod
    def get_products(self) -> Enum:
        """Must return Enum with products"""
        pass

    @abc.abstractmethod
    def find_product_by_name(self, product_name: str) -> Optional[int]:
        """Must return product_id or None if not exists"""
        pass

    @abc.abstractmethod
    def add_order(self, product_id: int) -> int:
        """Must add record with product_id into orders and return inserted record id"""
        pass

    @abc.abstractmethod
    def get_bill(self, order_id: int) -> Optional[BillSchema]:
        """Must get bill of order_id or None if order doesn`t exists"""
        pass

    @abc.abstractmethod
    def order_payed(self, order_id: int) -> bool:
        """Update status of order to payed and return is success"""
        pass

    @abc.abstractmethod
    def get_not_processed_orders(self) -> OrdersListSchema:
        """Get only not processed orders"""
        pass

    @abc.abstractmethod
    def processed_order(self, order_id: int) -> bool:
        """Change status of order to PROCESSED"""
        pass

    @abc.abstractmethod
    def get_orders(self, date_from: datetime, date_to: datetime) -> List:
        """Get all orders from date_from to date_to"""
        pass

    @abc.abstractmethod
    def add_product(self, product):
        pass

    @abc.abstractmethod
    def get_staff(self, username: str) -> Optional[StaffInDb]:
        pass

    @abc.abstractmethod
    def create_new_staff(self, username: str, password: str, position: int) -> bool:
        pass

    @abc.abstractmethod
    def delete_staff(self, username: str):
        pass

    @abc.abstractmethod
    def delete_product(self, product_name: str):
        pass

    @abc.abstractmethod
    def delete_order(self, order_id: int):
        pass
