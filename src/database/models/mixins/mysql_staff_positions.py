from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import MEDIUMINT


class MysqlStaffPositions:
    POSITION_CASHIER = 0
    POSITION_SHOP_ASSISTANT = 1
    POSITION_ACCOUNTANT = 2

    position = Column(
        "position",
        MEDIUMINT(unsigned=True),
        index=True,
        unique=False,
        nullable=False,
    )
