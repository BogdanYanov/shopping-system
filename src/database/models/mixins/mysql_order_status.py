from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import MEDIUMINT


class MysqlOrderStatus:
    STATUS_NEW = 0
    STATUS_PROCESSED = 1
    STATUS_PAID = 2

    status = Column(
        "status",
        MEDIUMINT(unsigned=True),
        index=True,
        unique=False,
        nullable=False,
        server_default=text("0"),
    )
