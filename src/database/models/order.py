from sqlalchemy import Column
from sqlalchemy.dialects.mysql import BIGINT, TIMESTAMP

from .base import Base
from .mixins import JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin, MysqlOrderStatus


class Order(Base, JSONSerializable, MysqlPrimaryKeyMixin, MysqlTimestampsMixin, MysqlOrderStatus):
    __tablename__ = "orders"

    product_id = Column(BIGINT(unsigned=True), nullable=False)
    bill_created_at = Column(TIMESTAMP, nullable=True)
