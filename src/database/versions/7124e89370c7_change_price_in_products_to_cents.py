"""change price in products to cents

Revision ID: 7124e89370c7
Revises: 7b8ac7b98d9e
Create Date: 2020-10-26 09:39:48.437539

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from database.connection import engine

# revision identifiers, used by Alembic.
revision = '7124e89370c7'
down_revision = '7b8ac7b98d9e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    sql_raw = """
            ALTER TABLE products
            ADD COLUMN price_cents BIGINT UNSIGNED not null after name;
        """
    op.drop_column('products', 'price')
    engine.connect().execute(sql_raw)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    sql_raw = """
                ALTER TABLE products
                ADD COLUMN price DECIMAL(12, 8) not null after name;
            """
    op.drop_column('products', 'price_cents')
    engine.connect().execute(sql_raw)
    # ### end Alembic commands ###
