from sqlalchemy import create_engine
from tools import mysql_connection_string

engine = create_engine(mysql_connection_string())
