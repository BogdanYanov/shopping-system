class NoProducts(Exception):
    def __init__(self):
        super().__init__("No products in database")