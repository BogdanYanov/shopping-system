from fastapi import FastAPI
from api.routes import cashier_router, seller_router, accountant_router, overall_router, admin_router

app = FastAPI()

app.include_router(cashier_router, tags=['cashier'])
app.include_router(seller_router, tags=['seller'])
app.include_router(accountant_router, tags=['accountant'])
app.include_router(overall_router, tags=['overall'])
app.include_router(admin_router, tags=['admin'])

