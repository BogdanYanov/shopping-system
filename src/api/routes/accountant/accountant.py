from fastapi.routing import APIRouter
from datetime import datetime, date
from api.repository import repository
from api.schemas import OrderListExtSchema
from auth.models import StaffInDb
from fastapi import Security
from api.authenticator import authenticator


router = APIRouter()


@router.get('/all-orders', response_model=OrderListExtSchema)
async def get_all_orders(
        date_from: date,
        date_to: date = datetime.now().date(),
        current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['accountant'])
):
    """
        Return all orders for date range (date_from - date_to). Work only for Accountant profile
    """
    return repository.get_orders(date_from, date_to)
