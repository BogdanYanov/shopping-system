from fastapi.routing import APIRouter
from api.schemas import OrdersListSchema, MessageResponse
from api.repository import repository
from fastapi import HTTPException, status, Security
from auth.models import StaffInDb
from api.authenticator import authenticator


router = APIRouter()


@router.get("/orders-np", response_model=OrdersListSchema)
async def get_not_processed_orders(
    current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['shop_assistant'])
) -> OrdersListSchema:
    """
        Return not processed orders list. Work only for Shop Assistant (seller) account type.
    """
    return repository.get_not_processed_orders()


@router.patch("/order/{order_id}", response_model=MessageResponse)
async def processed_order(
        order_id: int,
        current_user: StaffInDb = Security(authenticator.get_current_user, scopes=['shop_assistant'])
) -> MessageResponse:
    """
        Set status PROCESSED to order by {order_id}. Work only for Shop Assistant (seller) account type.
    """
    if repository.processed_order(order_id):
        return MessageResponse(message="order has been processed")
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Order not found, or already has been processed"
    )
