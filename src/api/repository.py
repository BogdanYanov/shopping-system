from database.repository import MySQLRepository
from database.connection import engine

repository = MySQLRepository(engine)