from pydantic import BaseModel
from typing import List
from datetime import datetime


class OrderSchema(BaseModel):
    order_id: int
    created_at: datetime


class OrdersListSchema(BaseModel):
    orders: List[OrderSchema]

    class Config:
        schema_extra = {
            "example": {
                "orders": [
                    {
                      "order_id": 1,
                      "created_at": "2020-10-26T14:58:56"
                    },
                    {
                      "order_id": 2,
                      "created_at": "2020-10-26T14:58:58"
                    },
                    {
                      "order_id": 3,
                      "created_at": "2020-10-26T14:58:59"
                    }
                ]
            }
        }

