from pydantic import BaseModel
from datetime import datetime
from typing import List


class OrderExtSchema(BaseModel):
    name: str
    price: str
    status: str
    order_created_at: datetime


class OrderListExtSchema(BaseModel):
    orders: List[OrderExtSchema]

    class Config:
        schema_extra = {
            "example": {
                "orders": [
                    {
                        "name": "IPHONE XR",
                        "price": "$ 12345.00",
                        "status": "new",
                        "order_created_at": "2020-10-26T14:58:58"
                    },
                    {
                        "name": "IPHONE XR",
                        "price": "$ 12345.00",
                        "status": "paid",
                        "order_created_at": "2020-10-26T14:58:59"
                    },
                    {
                        "name": "IPHONE XR",
                        "price": "$ 12345.00",
                        "status": "processed",
                        "order_created_at": "2020-10-26T14:59:00"
                    }
                ]
            }
        }
