import pytest
from api.repository import repository
from database.models.mixins import MysqlStaffPositions
from api.authenticator import authenticator
from fastapi import status
from urllib.parse import quote


@pytest.fixture
def create_cashier():
    is_staff_created = repository.create_new_staff('testcashier',
                                                   authenticator.get_password_hash('testcashier'),
                                                   MysqlStaffPositions.POSITION_CASHIER)
    assert is_staff_created is True
    yield
    repository.delete_staff('testcashier')


@pytest.fixture
def add_product():
    repository.add_product({'name': 'Test Product', 'price_cents': 1290000})
    yield
    repository.delete_product('Test Product')


@pytest.mark.cashier
def test_create_order(client, create_cashier, add_product):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testcashier",
                                       "password": "testcashier",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    response_create_order = client.post(f'/order?product={quote("Test Product")}',
                                        headers={"Authorization": f"Bearer {access_token}"})
    assert response_create_order.status_code == status.HTTP_200_OK


@pytest.mark.cashier
def test_unexist_product_order(client, create_cashier):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testcashier",
                                       "password": "testcashier",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    response_create_order = client.post(f'/order?product={quote("Test Product New")}',
                                        headers={"Authorization": f"Bearer {access_token}"})
    assert response_create_order.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.cashier
def test_permission_cashier(client, create_cashier):
    response_login = client.post('/login',
                                 headers={"Content-Type": "application/x-www-form-urlencoded"},
                                 data={"username": "testcashier",
                                       "password": "testcashier",
                                       "grant_type": "",
                                       "scope": "",
                                       "client_id": "",
                                       "client_secret": ""})

    assert response_login.status_code == status.HTTP_200_OK
    access_token = response_login.json()['access_token']
    auth_header = {"Authorization": f"Bearer {access_token}"}
    response_bill = client.get(f'/bill/1',
                               headers=auth_header)
    assert response_bill.status_code in [status.HTTP_404_NOT_FOUND, status.HTTP_200_OK]
    response_np_orders = client.get(f'/orders-np',
                                    headers=auth_header)
    assert response_np_orders.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_np_orders.json() == {"detail": "Not enough permissions"}
    response_all_orders = client.get(f'/all-orders?date_from=2020-10-24&date_to=2020-10-28',
                                     headers=auth_header)
    assert response_all_orders.status_code == status.HTTP_401_UNAUTHORIZED
    assert response_all_orders.json() == {"detail": "Not enough permissions"}
