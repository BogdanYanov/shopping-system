import pytest
from fastapi.testclient import TestClient
import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')
from api.app import app


@pytest.fixture(scope='session')
def client():
    client = TestClient(app)

    yield client
