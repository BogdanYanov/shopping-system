#!/bin/bash
set -eu
cd /var/app/python/src
export PYTHONPATH=/var/app/python/src
poetry config --local virtualenvs.in-project true
poetry install
poetry run alembic upgrade head
poetry run python /var/app/python/src/load_fixture.py /var/app/python/src/products.json
poetry run uvicorn main:app --host 0.0.0.0 --port 80 --reload
